+++
title = "Contact Me"
description = "Vidhu Kant Sharma's contact information"

[extra]
nav_title = "VidhuKant's Contact"
+++

# Contact Me

*It's not like I want you to talk to me ba- baka!*

If you just want to respond to my blog or have any questions feel free to contact me.  
I might take a day or two to reply, and won't respond to any E-Mails that 
I think might risk my personal information.

## Public E-Mail Address:

[vidhukant@vidhukant.com](mailto:vidhukant@vidhukant.com)

## Find me on other services

- YouTube - [Vidhu Kant Sharma](https://youtube.com/@_VidhuKant)
- Mastodon - [\_VidhuKant](https://ohai.social/@_VidhuKant)
- GitHub - [VidhuKant](https://github.com/VidhuKant)
- GitLab - [VidhuKant](https://gitlab.com/VidhuKant)
- Twitter (Future X) - [\_vidhukant](https://twitter.com/_vidhukant)
- MyAnimeList - [0ZeroTsu](https://myanimelist.net/profile/0ZeroTsu)
- Odysee - [MikunoNaka](https://odysee.com/@MikunoNaka)
