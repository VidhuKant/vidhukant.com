+++
+++

# Welcome to my little corner on the interwebs

You modified some beeps and boops on your computer, that modified some beeps and boops on my computer
then it modified some more beeps and boops on your computer and now you're reading this. How cute

## Who am I?

{{ img(src="/pics/vidhukant.webp", alt="My photo", caption="What I look like (probably idk)", title="Vidhu Kant Sharma", height="267", width="400") }}

I am a human being who:

- kinda likes technology
- loves coffee (not a caffiene addict I swear)
- kinda doesn't like technology
- makes vague lists
- can play the e-minor chord, maybe
- can speak a little bit of japanese
- likes J-pop (suprise surprise!)
- cannot play the guitar
- likes to read

My name is Vidhu Kant Sharma but I like to write it as "VidhuKant" (stylized) because why not.
This is a great test to see if you even pay attention to the shit that I say!
