+++
title = "Neofetch is NOT dead"
description = "Neofetch was recently archived and hasn't seen any development in 3 years, but that does not mean it's dead. Open source projects don't die that easily"
date =  2024-05-06T02:43:00+05:30

[taxonomies]
tags = ["100DaysToOffload", "foss"]
+++

In my [last post](/blog/neofetch-is-dead/) I talked about the archival of neofetch's repository.
But that definitely doesn't mean the end of neofetch.

dylanaraps, the creator of neofetch recently updated his github readme saying that 
[he's a farmer now](https://github.com/dylanaraps/dylanaraps/commit/811599cc564418e242f23a11082299323e7f62f8)
and the reactions are pretty hilarious. He archived all of his public repositories on 26th April 2024
which is a clear indication that -whether he's a farmer now or not- he has no intentions to continue development
of those projects.

But that doesn't mean that neofetch, and his other projects are dead now. It takes more for a project to "die".

It's not unknown that:
1. Neofetch is open source.
    Open source software doesn't die easily. If someone is willing to maintain it they can *easily* fork 
    the project and continue development.
2. Neofetch hasn't seen any development in the past 3 years.
    The last commit was in December of 2023, yet many people don't even know this and continue to use
    neofetch. That's precisely because this project is feature complete, and doesn't really need 
    any active development.

This definitely isn't the end for neofetch. Same applies to all the "dead" projects on GitHub and other
platforms. Just because something doesn't get active development, or any development at all, doesn't mean
it's useless now. Software just doesn't break out of nowhere if you don't change anything! Especially 
something so simple as a shell script.

*This is post 03 of [#100DaysToOffload](/tags/100daystooffload/)*
