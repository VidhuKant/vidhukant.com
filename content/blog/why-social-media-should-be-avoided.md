+++
title = "Why Social Media Should Be Avoided"
description = "Social media is very good at manipulating your views. You won't realise but your thinking gets greatly affected by it"
date = 2022-07-15

[taxonomies]
tags = ["rant"]
+++

Welcome to Vidhu Kant's Based and Redpilled Blog. Enjoy your time here.  
If you have any opinions to share I'd be more than happy to hear.

This one's gonna be the legendary, shitting on social media blog post.

# Social media is evil and is using you.

Okay, I will try my best to not be extreme and try to explain this topic in layman's terms.
Basically, there are way too many bad things with social media. And I don't see those flaws going away anytime soon.
There is no "one flaw" there are many and you need to look at these said flaws in different perspectives to understand what I mean.

I won't be covering much about the "privacy" part (I'd love to but that's what everyone does), 
I'll be explaining how social media usage is bad for your mental health and is very bad for the society. 
There are many more arguments one can make but I'll leave that up to those people who are smarter than me (especially in the fields I don't cover here)

For clarification I will say, I'm not including messaging apps with my reasoning though many things (especially in the privacy section) do apply there.
I personally think they *are* somewhat important. I don't like using them but you can't just leave some of these services. 
(You can take that as an excuse, but I stand with my point)

## Glossary

- SNS: Social Networking Service
- PII: Publically Identifiable Information

## Social media can manipulate your views.

Social media is very good at manipulating your views. You won't realise but your thinking gets greatly affected by [SNS](#glossary).

### Their recommendation system plays with your mentality

These social networking sites are really good at analysing your likes and dislikes, and manipulate the recommendations shown to you based on that.
It's a very convenient and honestly kinda cool system. But it's also very dangerous.

This kind of system that adapts to your recommendations would make you fall into a rabbit hole and would make you believe in this one thing and one thing only.
You'd watch some kind of conspiracy videos and YouTube would recommend more of them. If you're not being careful and are in a vulnerable state of mind
you'd really start believing in those kinds of things. This is actually a really dangerous thing. On the surface it might just seem like a cool system
which would help you find something like, music that really matches your tastes. But this kind of system can very easily destroy your mindset.
I'm not saying this has the potential to do this kind of thing. If you pay attention you might spot victims of this *pretty easily*.

### Our mind seeks validation, social media provides that in excess

Seeking validation is a human trait, and I'm not saying that it's a bad thing. When you do some task you'd try to seek others to 
give you a compliment, or to praise you.
Which is a basic human instinct, but social media gives us an incredible amount of validation, which is literally like a trap.
They want to give you the dopamine rush you need. It doesn't seem too bad at the start but soon you'll realise that you are addicted to it.
All these "challenges" and "social experiments" and other trends that require your participation, they try to keep you in the community and make sure that you
don't leave.

This system is designed in this way because they want user retention. You are literally generating the big tech companies money by using their platform.
And in return you really are not getting anything useful. They try to understand what you like, and collect your data, to fine-tune the ads shown to you.
To all these big social media companies you're just a slave that can't leave their product, and generates you money and fame.

### Social media forces you to follow what's mainstream

This goes with many things. Some brand that manufactures something, some k-pop group, whatever.
Social media has made it so that you just have to follow mainstream things. It keeps ~~shilling~~ promoting all these products,
and these "influencers" promoting random garbage makes you think that you really need to spend money on this thing that you really don't need.

I'm not saying either of these "influencers" or the advertisers are wrong, this is just how social media works, and they know it.
Everything seems to be monetised at this point. Most of the popular social media accounts are just dedicated to shilling all this mainstream media, 
mainstream products. And, really, pressuring you to spend your time/money on them too. Which sadly seems to be normal at this point, but:

#### Social media has taken away most people's personality at this point

This is a very, very personal opinion of mine. Most of the social media users I know, (I mean everyone uses SNS at this point, but I'm talking about the hardcore users)
are living this constant fight to consoom as much memes, and as much information they can. It's like if you haven't followed this particular account or don't like this
meme you'd be exiled or some shit.

Also, everyone just wants to copy others. They try to copy some famous YouTuber to become the center of attention, things like that. (copying should not be confused 
with taking inspiration and immitating others to learn from them. Which kind of is a thing in the YouTube community)

It almost seems like people don't even have a personality anymore. They are supposed to believe this one opinion. If you don't agree or do something else twitter
is literally going to "cancel" you. This is dumb. And this is a reality.

### Social media takes away your freedom

Most social networking services are owned by private companies, they don't care about you. They care about money. And you are a just a tool to make them money.
To even look at a post they want you to sign up. You need to give your PII (Publically Identifiable Information) in order to register with their services.

You can't just opt out of them not collecting your data. You need to give them some of your most sensitive private info to even use their service.
They put unnecessary regulations on you and demand you to share your personal data. You don't have the freedom to choose what is visible to others and what is
kept private. And then they put up vague rules in their "content policy" to keep you confused. Suppose you post something and it gets removed, they won't
tell you why it got removed. They just give a vague reply like "this post goes against our terms of service/community standards". They don't give you a reason. 
They want to keep you confused and afraid that you won't make this mistake again lest they terminate your account.

There are a LOT of platforms that take away your freedom of speech in various ways. They won't allow you to say anything that goes against the moderators' personal
beliefs. This in a way engineers the society to also agree to whatever they want them to believe.

### Social media gives you false hope

I've seen this case irl with a few people and it infuriates me.
You might think that these bullshit services teach you life hacks and different things, because "internet is a great way to share information"
which it is, but social media definitely not one of those places.

People absolutely believe that scrolling through instagram would give them access to news, and facts, and various scientific knowledge which also trains their mind
to believe that there's nothing wrong with using instagram (or whatever). It doesn't cross their mind that they might be making use of this particular service/app
a bit too much. And social media is designed that way. Again, it's supposed to keep you on it. It'd brainwash you into believeing that what you are doing is right
and you should never leave this particular service. In my opinion these "big tech" are just abusing their users. Manipulating them into using their service more
and more and get them addicted to it.

I cannot stress this enough. To big tech you are a tool to generate money (and fish personal data out of). You should avoid them whenever you can. You should always
be in moderation of what services you use and what personal data you share. You are much better than this. Don't fall into their trap.  
It's your right to be in control of your personal data and they take it away. 

## ~~Social media~~ most of the services on the internet strip away your privacy.

Apart from the psychological aspect, they are collecting your data. Which can be considered a massive breach to your privacy.

I like to call the modern internet a "privacy shithole". Every other big website is just after your personal data. And this "I have nothing to hide" 
and "Privacy is for criminals" mindset is very toxic towards yourself and is making the situation worse.

### Before allowing a site/app access to your device, decide if it really needs it

Whenever you install an app on your phone, or visit a website, they ask you for various permissions. Look at them and think about it - does said app/website
really needs that access? And do you want to allow it?

Take facebook for an example, when you install the app, it (at least on android) asks for a bunch of "App Permissions". Those include things like

- Microphone access
- Location access
- Camera access
- Filesystem access
- Gyroscope access
- many more (I don't use fb so I don't fully know)

This seems pretty normal, most people are familiar with the "allow \_\_\_\_ access to \_\_\_\_?" message. Many even click allow without even reading it. 
Which is by far one of the worst things you can do with your phone.

When you see such message, think. *Do you really want facebook to have access to your microphone? Why would it use the mic?*
Surely if you are using the calling or video recording feature you might need mic access. *But are you even using it?*  
If not, do NOT give mic access. It's your right to decline mic access to them. They are spying on you and you should take an initiative to go against them.

I'm saying this a lot, but this is a really effective way how facebook fishes out your data. It's listening to you. 
While you are using their app they would record your audio and analyse that, to generate better ad recommendations for you. 
It's totally a system designed to act against you. It's a very dangerous thing that they are recording your personal data. 
Things you might not want anyone to know, they are recording it all. You should not use such apps. It's a dangerous concept.

Your data needs to be private. Even if you think you "have nothing to hide", hear me out:  
**They are making money with YOUR personal information.**
And you aren't even getting anything in return! What did they give to you? Mental stress? Jealousy? Desire to spend money/time/mental energy on something useless?  
Trust me, social media is not worth it for its downsides.

# But how are we supposed to exchange information on the internet?

Normally I'd explain how easy it is to set up a website (without even knowing any programming!) but this is supposed to be an article for *everyone*, 
wether you understand tech or not. ([tell me](/contact/) if I managed to do that!) So while I don't 100% agree with it, I'd recommend everyone to use social media under moderation, and try to minimise the usage, 
share as little personal information as possible. But really I'd just say - don't use social media! Don't base your lifestyle around it. 
There are better things out there. There are many more things to explore on the internet!
The internet is full of very informative websites without the garbage monetization and useless features included with social media.
But you need to explore. Try out <https://wiby.me>, it's a search engine which only allows personal, small websites without many ads, etc.
If you know other websites without unnecessary trackers, etc, you can also submit them to [wiby](https://wiby.me)!

## Advice for organisations/businesses

Also, this is advice for organisations/businesses, etc. Set up your own websites. You can totally do that. It doesn't have to be one with a very modern UI.
Focus more on the content, not aesthetics (but also don't make it an eyesore!). 
Make your users want to re-visit your website. Host all the useful info related to your brand on the website.
When someone asks, just give them your website. You are free to do whatever with your site, you don't have to comply with any other service's terms of use, 
and it just looks MUCH more professional!
