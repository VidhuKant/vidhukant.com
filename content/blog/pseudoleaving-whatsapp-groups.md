+++
title = "LPT: Pseudoleaving annoying WhatsApp groups"
description = "Archiving a whatsapp group isn't enough. Pseudoleave an unwanted group to peacefully get the illusion that you are no longer a participant."
date = 2024-02-28

[taxonomies]
tags = ["guide", "hacks", "tips"]
+++

Group chats are cringe. Especially if you've been involuntarily added to them.
I have way too many from college which usually get junk messages but I can't really leave them.

Well, it's a valid option to just archive it; but then you have that annoying "Archived" section. It kills the vibe!

## The solution

So I'm gonna teach you guys a pro gamer move, which I call, *pseudoleaving*.

### Step 1

Mute the group chat.

### Step 2

Lock the group chat.

### Profit

It's as simple as that! 
Unless you often use the chat lock option, you now have the illusion that you've left the group!
