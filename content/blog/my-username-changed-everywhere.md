+++
title = "Why my username is no longer MikunoNaka"
description = "MikunoNaka's no more. VidhuKant supremacy!!!"
date =  2024-05-07T06:03:00+05:30

[taxonomies]
tags = ["100DaysToOffload"]
+++

If you somehow came from my YouTube channel or GitHub profile, you might know 
that my username almost everywhere was MikunoNaka. I even had a domain with that name.

For about an year I've been thinking about changing that, because of reasons 
we'll talk about later. But I never did that because I already had a domain 
called "mikunonaka.net" (disclaimer: I no longer own it) so I thought since I 
have already spent money on it, better to keep the username I guess.

## Why MikunoNaka was a bad username

### The meaning behind was pretty cringe

I'm only going to say this once, and will be linking this just in case someone 
asks. I'm cringing my ass off right now.

*MikunoNaka is an anagram for "Nakano Miku"*

{{ img(src="/pics/nakano-miku-07may2024.webp", alt="Nakano Miku cute pic", caption="<em>I don't own the rights to this image and got this from google. <br>Please contact me for removal</em>", title="Nakano Miku") }}

I mean I was like, 14 and also an idiot when I thought this up

### It was hard to spell

I wanted an easy to remember name. MikunoNaka was awkward to type and no one really 
remembered its spelling. 

I didn't care back then but later I started to host my git repos on 
mikunonaka.net, and was talking to my friend about some project, and he tried to 
open the repo by typing up mikunonaka.net and he struggled to remember the URL.
And this happened on multiple occasions. 

> Was it mikununuka?  
> Mikumaka?  
> The N is uppercase? Why?  

That guy knows about mikunonaka.net. It was just that hard for even him 
to remember. I didn't want to waste my money on such a domain name.

### Was hard to pronounce

I mean how am I going to explain to people how to say "MikunoNaka" and not weird 
them out! It sounds weeb-ish (it is but idc) but also wery awkward to tell others. 
Again wasn't an issue but how will I tell them to visit my channel or github or 
even twitter, etc if they don't even understand what I'm saying

### I no longer have mikunonaka.net

This is the biggest reason. I already started hating this name, but I had a domain 
with this name so I thought I'd stick with it. That was, until I looked at the 
renewal fee. It's not a lot since domains are pretty cheap, but it's still money 
that'd be wasted on something I don't like. 

I think letting go of a domain can be somewhat risky. I'm not sure if it really 
is that big of an issue but what if I just didnt change my GitHub name, and 
someone registered mikunonaka.net for nefarious purposes. Since I already had it 
listed on many profiles, if someone saw that my name on GitHub is MikunoNaka, 
they'd probably think it's mine at first. That could go so wrong. I might just be 
paranoid but better safe than sorry.

## My name's better

In the end, I think my name actually sounds pretty cool. I'd rather people refer
to me as my real name nowadays. So I just use VidhuKant or a variation nowadays 
online unless I don't want to share my name.

## Conclusion

Everyone picks shitty names. Especially me as I am terrible at naming stuff. Have
you ever had a username which was pretty bad but was everywhere and you had this
dilemma about changing it or keeping it since it's everywhere?

*This is post 04 of [#100DaysToOffload](/tags/100daystooffload/)*
