+++
title = "Distributing a GTK App for Windows"
description = "How to build and distribute a GTK application for windows with msys2"
date = 2023-01-02
highlight = true

[taxonomies]
tags = ["guide", "programming"]
+++

# Setting up msys2

## Installation using chocolatey

You need to install msys2 before you can start developing on windows. 
The easiest way to set it up is to use the [chocolatey package manager](https://chocolatey.org).

If you don't already have choco installed, just follow the installation instructions on <https://chocolatey.org/install> then run

``` fish
choco install msys2
```

After msys2 is set up, open a new (non-administrator) powershell window, and in that run msys2

## Installing necessary packages

``` fish
sudo pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-toolchain base-devel glib2-devel
```

Refer to the installation instructions on GTK's website on which other packages you might need.
I believe this is enough to get started.

# Compiling your app

Now, clone your project's repository and build the app as you normally would on linux and place the executable in the `bin` 
directory inside your installation directory. Use the ldd command (`ldd bin/MyApp.exe`) to list the necessary DLLs required 
by your app and copy all the DLLs starting with `"/mingw"` to the `bin` directory. This script automatically does that:

``` fish
ldd bin/MyApp.exe | grep '\/mingw.*\.dll' -o | xargs -I{} cp "{}" ./bin
```

Credit goes to <https://stackoverflow.com/a/50130668>; Replace MyApp.exe with your executable's name.

# Adding other required files

## Icon Theme

Create a directory for the icon theme with `mkdir -p ./share/icons`, and download the hicolor (mandatory) and another icon pack (I'll be using Adwaita) 
and place both files into `share/icons`.

Or just copy from your mingw installation

``` fish
cp /mingw64/share/icons/* -r share/icons/
```

## GLib Schemas

Settings schemas need to be compiled in order for the app to work. The quickest way is to just copy the default ones from your mingw installation.
In a real-world scenaro you might want to edit these files and delete any irrelevant settings but just for testing I don't care.

``` fish
mkdir -p share/glib-2.0/schemas
cp /mingw64/share/glib-2.0/schemas/* share/glib-2.0/schemas/
glib-compile-schemas.exe share/glib-2.0/schemas/
```

## Pixbuf loaders

GTK needs Pixbuf loaders to be able to load images, else it will crash. Just copy the loaders from your mingw installation:

``` fish
cp /mingw64/lib/gdk-pixbuf-2.0 -r lib/gdk-pixbuf-2.0
```

# Finishing up

Check that your app runs, and either zip the file or create a microsoft installer to distribute the app!
