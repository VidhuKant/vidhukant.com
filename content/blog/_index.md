+++
title = "VidhuKant's Blog"
sort_by = "date"
template = "blog/blog-index.html"
page_template = "blog/blog-post.html"
+++

# Welcome to my blog

I'm only getting started with regularly updating this stuff, constructive criticism is appreciated!
I write about.. anything but mainly developer stuff.

[Tags](/tags/) | [RSS Feed](https://vidhukant.com/feed) | [100DaysToOffload](/tags/100daystooffload/)
