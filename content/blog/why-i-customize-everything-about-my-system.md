+++
title = "Why I Try to Customize Everything About My System"
description = "I love customizing things. I always go out of my way to make sure my phone, or my laptop, is doing what I want, the way I want."
date = 2022-07-19

[taxonomies]
tags = ["opinion", "tech"]
+++

I love customizing things. I always go out of my way to make sure my phone, or my laptop, is doing what I want, the way I want.
Recommended usage? Pre-defined shortcuts? Don't tell me how to use my computer. I tell my computer how I want to use it. And make it adapt to that!

I like to make my desktop/phone look good (doesn't matter if other people find it pretty though), and I love it when everything is easily accessible and all that.
Moreover, I like when all my devices look similar, and work similarly. It starts from my laptop and my phone having an anime wallpaper to radiate max weeb energy,
but also includes everything being of my favourite color, etc.

## It's actually a learning experience

Going out of the way to customize software is actually a learning experience, if you're very serious about it, that is.
I learned to flash phones in like 2018 because I wanted to install a newer version of android on my phone, and now I only run custom ROMs on my phone. 
And that's for a very good reason.

All phones are built differently, and nowadays phone manufacturers add new features and remove old features for various reasons that I won't cover,
but basically 2 phones both running the same OS still have many differences. I don't like it because when I use a device (i.e a phone), I don't want to waste my
time learning some new system or missing some (software) feature because the manufacturer decided to remove it. This is why always use LineageOS 
(or another AOSP-based operating system, but Lineage is my preferred choice) on my phones. Really because I'm used to the UI, I love the Trebuchet launcher,
and the default ringtone is awesome (literally the only default ringtone that isn't cringe)

And this has taught me how to work with fastboot, twrp, etc to flash phones and install different operating systems on it. It's not very hard 
(but kinda dangerous for your device, I will say) to do but spending hours putting different OSs on my phone has helped me understand how 
phone operating systems really work.

Similarly, on the desktop side, my quest to get the best desktop experience, with an interface which works hand-in-hand with my thought process,
and everything being plastered with my favourite color, purple, I learned a lot about the command line, linux, etc.

So if you want to learn more about computers, I highly recommend trying to customize them. I.e tweak your desktop to your liking, try out different programs,
or (if you want to learn programming), try to build alternatives to the programs you already use, with a different UI or colorscheme that you like.

## It boosts my productivity

I use tiling window managers and CLI apps, because these are very configurable/extensible. This not only helps me understand how my operating system really works, 
but also lets me tweak everything to my liking and "match how I really think". What that means is if I like to work with the keyboard I can configure my system to 
be as keyboard-driven as possible so not only it tires my mind less, it really boosts my productivity. And this isn't something you can achieve with proprietary 
software since it's not very configurable. This is a very good reason to use only free and open source software since it boosts your productivity.

I'm not saying that oh you should use XMonad and spend hours configuring it. I'm saying you should try out different things, window managers, apps, whatever.
You should find out what suits your workflow and use that. Or, you can even fork/create one of your programs that fulfil one specific requirement you have. 
Trying out different things is always a good thing. And it really teaches you about the different ways a task can be performed on a computer.

Those "things" can be anything, right, maybe just switch to firefox if you use chrome (seriously, don't use chrome/opera/edge), or if you're on firefox install icecat.
Play around with different software, tweak it to your liking. See what differences the alternatives have. 
Maybe install a different operating system, try dual booting, just try out different stuff! And learn from it!

## Conclusion

I just want my phone/computer to do what I want. And I want the UI to match my tastes. And I think this kinda adds "personality" to my devices!
I think anyone who wants to learn computers should play around with the different settings, etc because this in my opinion is one of the best ways to 
understand how your system actually works.

Also, if you're a programmer (doesn't matter if you're learning) it could inspire you to work on different projects. Which is always helpful.
Personally, I learned shell scripting just because I wanted to share my linux desktop audio and mic input on discord at the same time, and I 
created a shell script for that! It solved my problem, and added shell scripting to my skill set! 
