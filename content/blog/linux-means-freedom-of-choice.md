+++
title = "Linux means freedom of choice... And so does not using it!"
description = "Linux elitists are wrong about being rude to people switching out of linux"
date = 2024-04-27

[taxonomies]
tags = ["linux", "opinion", "tech"]
+++

I count myself as a Linux enthusiast; I cannot function without telling people that I use Arch btw (/s)

## But why?

There're a bunch of reasons for me to use linux, it's more secure, more private, lightweight...  
BUT that's not the main reason. I started caring about stuff like this *after* I became familiar with the "Linux Ecosystem".
The dealbreaker for me was... Freedom of choice!

## Customizability

Customizability means A LOT to me. If something is mine it needs to scream its mine. 
Call me a narcissist but I brand everything I own with my own tastes! 
Since I spend a lot of time on my laptop, I want it to look exactly how I want it to look like 
(doesn't matter if others find it ugly) and I want it to support my workflow, not the other way around.

![My ArchLinux Setup](/pics/rice_screenshot-27042024.webp)

Now, linux gives me this freedom of choice to use whatever window manager I want to use,
whatever status bar I want to use, etc. This kind of thing would be hard to impossible on other 
proprietary operating systems! Now me using ArchLinux, with the XMonad window manager, and the
Dvorak keyboard layout doesn't mean these are the best choices. 
It's what I want to use, it wasn't offered to me, and I had the complete
freedom to just stick with KDE or something. (btw I love KDE) 

Now I try to encourage people into using Linux. It's not ideal for most people but if someone is
curious and/or tech savvy enough I always try to get them to use Linux. Obviously, most people
don't stick with it, but that's fine. Linux gives you the freedom of choice, and using an easy to
use distro, or not using Linux at all is also a choice!

But what icks me the most is, people refusing to even learn about Linux then hating on it. Same might
apply to a bunch of other things but not wanting to use Linux is more than fine by me.. But I think if someone
rejects something without even trying it, it's not a choice anymore. It's just stupidity and stubbornness.

So my whole point is, linux elitism is cringe and giving shit to people for switching out of linux is a horrible thing to do
yet, people hating on linux without ever trying it are just as bad.
