+++
title = "I'm switching to Dvorak..."
description = "As a vim and XMonad user I just switched to dvorak, and this has been my experience"
date = 2023-12-13

[taxonomies]
tags = ["tech", "typing"]
+++

久しぶり!!!

So I've finally taken the red pill and switched to the Dvorak Programmer layout;
and now you have to read about my experience *heh heh*.
Now whether Dvorak actually makes you more accurate and/or faster hasn't really been 
proven and I won't be talking about that, but facts & logic aint gonna change my decision now.

## Y tho

I am not ashamed to admit that the main reason for this was because it's cool and quirky.
The *other* main reason is that I wanted to get better at typing.
I, like most people, learned typing the wrong way. I started out by looking at the keyboard 
while typing, and slowly moved on to touch typing. but looking at the keyboard is bad 
for your posture and eventually reduces accuracy. Surely it's possible to learn the 
right way later on, but in my opinion it is just easier to forget everything and learn from 
the start... preferably the hard way! Now that's where layouts like Dvorak and Colemak 
come in handy.

### Why not colemak?

Dvorak (especially Programmer's Dvorak) seems more attractive as a programmer, 
but who knows, maybe I'll try out Colemak one day too!
I will say that Colemak seems more sensible for keyboard shortcuts but I hated
the default ones anyways. But I love how Dvorak has commonly used symbols 
in more accessible places.

## First thoughts

Initally it felt really hard but comfy in Google Docs (yes I sometimes have to use it) and
it was surprising how many words I can make just from the home row.
But when it came to other programs... Oh dude

It was hell. It's still hell. I cannot use my window manager, I cannot use Doom Emacs,
I cannot use vim. Using a new layout is confusing enough, but it's 10x worse when your 
brain just hits h,j,k,l without thinking. I somehow typed out a part of this article
in emacs but since "First Thoughts" I switched to vscodium just to keep my sanity.

## Second thoughts

Fuck my typing speed literally became 17% of what it used to be. It actually feels much 
more frustrating than I imagined. I literally cannot use my pc properly anymore.
I'm sure I'll get it back but for the time being typing this article on 14 WPM actually 
feels like shit. I'm literally taking way too many sanity breaks. I've been writing this
since yesterday!

On the plus side, `ctrl-t` and `ctrl-s` are soo much more comfier! I've already started 
to fix my muscle memory. Before, I used primarily the left shift and almost always the 
left ctrl key and with Qwerty I had to be constantly conscious about it in order to keep 
the usage of the left and right keys balanced, which was almost impossible! With Dvorak 
it's much easier because I'm building better habits from the start. That's not a Dvorak 
thing; that's a new layout thing.

## Does Dvorak help though

Well yes and no...
In the end, it all boils down to your level of practise. With proper training you can 
become the best typist while still using Qwerty. It's *you* who is improving! But
like I said, it's easier to start over with good habits rather than to try to forget 
older bad habits. It's a more miserable experience but it's a one-time investment.
Also, I think what helps more is applying blanking stickers on your keyboard in order 
to encourage you to not look at the keyboard. You *can* find ones meant to be applied 
on keyboards online, but I used some paper stickers I had lying around. It doesn't 
look as good since it's DIY but it gets the job done.

With that being said, I do agree that Dvorak is a superior layout. It requires much 
less finger movement to type the most commmon words and has many more combinations in 
the home row. Only caveat for me is that most programs have keybindings that only
make sense for Qwerty. Vim is obviously hell at start, I wonder what Gimp and Kdenlive
would be like.

### Qwerty is outdated!

Qwerty was made for typewriters. It deliberately spreads apart more commonly used 
letters, increasing finger movement. In the context of typewriters it makes sense 
because it prevents jamming. But for computers, It's pretty outdated and less ergonomic.
The `;` key under the little finger has seemed funky ever since I learned touch 
typing. Dvorak just makes better use of all the Fingers! Just the position of the
`s` key is a huge selling point for me.

While other layouts require serious commitment, they also put less strain on your 
Fingers! It's definitely isn't easy, and it's not for everyone. I've taken the red 
pill, would you take one too?  
Let me know!
