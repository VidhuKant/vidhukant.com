+++
title = "I'm starting 100 Days To Offload"
description = "I am attempting 100 Days To Offload which is a challenge in which you have to write 100 blog posts in the span of one year on your personal blog."
date =  2024-05-04T23:14:00+05:30

[taxonomies]
tags = ["100DaysToOffload", "updates"]
+++

[100DaysToOffload](https://100daystooffload.com/) is a challenge in which you have to write 
100 blog posts in the span of an year. I am really excited for it as even though I don't update
this website much, I enjoy writing about random stuff.

So this marks my first post of `#100DaysToOffload`, lets see how far we go.
From now on I'll be regularly writing about various things that interest me.
I recommend you to follow this challenge yourself and [let me know](/contact/) if you do!

If you like to regularly follow my progress I recommend you follow me on Mastodon ([@\_VidhuKant@ohai.social](https://ohai.social/@_VidhuKant))
and/or subscribe to my RSS feed by adding [this link to my RSS feed](https://vidhukant.com/feed) to your feed aggregator.

*This is post 01 of [#100DaysToOffload](/tags/100daystooffload/)*
