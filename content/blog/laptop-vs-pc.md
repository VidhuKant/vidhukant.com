+++
title = "Laptops are Good!"
description = "I like PCs more than laptops because of the power and control you have on the hardwware. But hey, laptops aren't that bad!"
date =  2024-05-18T13:55:00+05:30

[taxonomies]
tags = ["100DaysToOffload"]
+++

I consider myself a PC guy (though I don't have one) because I want control on my hardware. 
What I mean by that is I want to choose exactly the configuration I need which sometimes isn't 
possible with laptops. Not to mention laptops are less performant, have worse thermals and 
the battery life on a gaming laptop is always shit.

But there are some good things included with a portable yet fully featured PC!

Currently, I'm down with fever yet I am writing this post while tucked in my bed. I want to
thank this technology that helped me open up neovim to write this post, use zola to turn this
post into an HTML document, and use rsync to upload the HTML onto my web server, from the 
comfort of my bed!

Surely there are ways to write a blog from phone (not that I need to write a post when I am sick
but I want to) but I prefer typing on a physical keyboard whenever possible. I just am not good
with touch screen technology, I always want physical feedback. If I had a PC I'd have to get to
my desk in order to write this post. Which I don't mind at all most of the times but right now
I just have enough energy to pass the time by doing shit on my laptop, but I honestly would rather
die than get up because my whole body hurts when I move.

Yet I'm hella bored and will now continue to learn rust. That too, from the comfort of my bed.

So with that, thank you laptops!

*This is post 05 of [#100DaysToOffload](/tags/100daystooffload/)*
