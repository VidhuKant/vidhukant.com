+++
title = "My Failed Attempt at Installing Gentoo"
description = "As a linux \"power user\" even I had so many problems getting Gentoo to work on my gaming laptop, I'd rather stick to ArchLinux for now"
date = 2022-08-02

[taxonomies]
tags = ["linux", "tech"]
+++

After using Arch Linux for slightly more than 2 years I decided to move to Gentoo.
I managed to install it (well it wasn't hard) but still way too many things went wrong;
and I'm gonna share my suffering with you guys by talking about it.

## Why I moved back to Arch Linux

Gentoo is actually really interesting, waiting 2 hours for portage to install your terminal really is a feel.
Though turns out that compiling the terminal didn't take that long, THAT SHIT ALSO COMPILED THE FUCKING PROGRAMMING LANGUAGE. 
Yeah, I compiled rust THEN compiled alacritty because that's what pro hackers do.
(I really thought it'd automatically pull the binary, apparently that's rust-bin not rust)

I like this, you compile things with the support for only what you need, and disable what you don't. It's not like Arch, 
you literally don't have support for the stuff you don't need, which is even faster! 
But I guess I still am not fit for that kind of pro gamer level stuff, so I'll stick to arch.

## Not even an X server worked 

Maybe it's just because I'm a dumbass, but I can't even get an X server running. Also wifi refused to work. 
I ran 2 floors to get an ethernet cable at 3 am and that also refused to work! I re-compiled my kernel with networking support and it didn't work!
I guess I should have read the logs but no, like any smart arch user would do, I re-installed it! How fun!

Over the course of 4 days I installed Gentoo 4 times. 2 of those 4 attempts even occoured in the same day.
I guess getting everything to work would've been easier, but I realised that too late. 
If you're planning to install gentoo, remember that it's easier to fix gentoo, a re-install would take too long.

## Installation wasn't hard at all!

Okay, to me, the installation part really wasn't too hard. I mean on Arch you just need to copy-paste a couple of commands, 
it's really the same with gentoo but.. easier somehow. I didn't have to run many shell commands, I just had a bunch of config files to play around
to fine-tune my system. It was time consuming, but not harder than Arch. I'd say some things were *easier* because instead of setting some variables myself 
I just used the `eselect` command to set locales and stuff, I guess that's kinda interactive and "less boring"

Overall I'd say, no installing gentoo is not hard at all. Especially if you've installed Arch before. BUT using it was pretty hard.. 
I had to deal with a lot of masked packages, and had to wait more than an hours just so I can install Xorg; I didn't even bother getting XMonad to work, 
just installed dwm because that compiles within seconds.

Also, while compiling a kernel isn't hard at all (I mean you just get an ncurses program with shit loads of options), but figuring out what I need to get
various things to work was pretty tricky, not to mention that I wasn't able to even do that successfully!

## I will go back to gentoo but not now..

Okay, I quit because I had wasted a lot of my time, and was tired and fed up of it. I **will** go back to installing gentoo on my main machine.
To anyone reading this interested in gentoo, I am NOT saying that gentoo is a bad distro. I'm just saying that it's very frustrating to install,
takes an incredibly long time, and by the time you install it you don't get left with any mental energy to fix any bugs.

I hope next time I try (maybe in a few months) I will be able to set up XMonad and polybar and all that cool stuff just like I have on Arch
or maybe have an even better setup, and maybe never even look back to Arch! But not now, I'm tired and will work on other stuff for now.

## A little gentoo wallpaper I made

I made this funny wallpaper to use with my gentoo installation (while my kernel was compiling in the background!)

[![Funny Gentoo Wallpaper](/pics/Gentoo\_Wallpaper\_OreImo.png)](/pics/Gentoo\_Wallpaper\_OreImo.png)

But sadly I wasn't able to use it. *Some day...*
