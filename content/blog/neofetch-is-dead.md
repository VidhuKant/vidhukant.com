+++
title = "The death of neofetch?"
description = "My reaction to the archival of the neofetch GitHub repository"
date =  2024-05-05T12:30:00+05:30

[taxonomies]
tags = ["100DaysToOffload", "foss"]
+++

I was browsing through YouTube when I came across this devastating video by 
[Andrea Borman](https://youtu.be/yixseXhg5ZY?si=t1Q5yglht3cFwrWz). In this video she 
talks about the [neofetch GitHub repo](https://github.com/dylanaraps/neofetch) being recently archived.
I recommend checking her channel out as she is very based (even though I myself don't watch her much)

If you are familiar with Linux you might've heard about neofetch. Neofetch is a shell program that 
shows some basic system information among other things. It is adored by the ArchLinux community and 
some even say neofetch helped them get through some of the worst times in their life.

As an Arch user myself neofetch just has helped me so much in life. Just running `clear && neofetch` in the 
terminal would take the deepest of the deepest pain away. When I was sad and lonely neofetch was there.
When I was happy neofetch was there. But now, now I have to deal with the pain myself. But thanks to
neofetch, I am a much stronger person now. I think I can handle it myself.

## About the man behind neofetch

Oh what a legend [dylanaraps](https://github.com/dylanaraps) was! He was the man behind neofetch
and many other popular open source programs.

According to [this commit](https://github.com/dylanaraps/dylanaraps/commit/811599cc564418e242f23a11082299323e7f62f8) on his
profile's repo, he has left development for good. I wish him the best. Jokes aside, if this is real, it's very based.
I'd like to imagine he is now contributing under a different name but we'll probably never know.

P.S. if you haven't realized yet, this (mostly) is satire. Neofetch hasn't seen any development since 2021 yet it works perfectly fine.
It's archived but definitely not dead, but that's a discussion for another day. I myself don't use it but it's still better than a blank 
terminal aesthetic-wise. Wherever dylanaraps is, I wish him the best. Based move.


*This is post 02 of [#100DaysToOffload](/tags/100daystooffload/)*
