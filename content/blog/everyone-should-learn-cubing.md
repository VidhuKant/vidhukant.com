+++
title = "Everyone Should Learn Cubing"
description = "Solving a Rubik's Cube sounds like a daunting task, and that's what I thought when I started learning a little more than one year ago."
date = 2022-09-21

[taxonomies]
tags = ["opinion", "other"]
+++

So I found my speedcube lying in my desk drawer and decided to get back into cubing and I'm hooked again!
Words cannot describe how fun this is. Solving a Rubik's Cube sounds like a daunting task, 
and that's what I thought when I started learning a little more than one year ago.

But it's actually really easy unless you want to be able to solve a cube in like, 30 seconds.
You just need to remember some cases and algorithms to apply with those cases. It's a great mind exercise,
and overall really fun. After you get comfortable you can just play some music in the background
and solve your cube, maybe challenge yourself and try to be as fast as you can while solving it; or make
various patterns on your cube!

![Indian flag pattern on a 3x3 cube](/pics/cube_indianflag.webp)
![Japanese flag pattern on a 3x3 cube](/pics/cube_japaneseflag.webp)
![Alternating colors pattern on a 3x3 cube](/pics/cube_mosaicpattern.webp)

I know I take extremely pretty pictures!

## The Rubik's Cube is one of the most fun puzzles out there

I love the cube. It's really fun, and you can just vibe with your cube. It's compact, you can take it anywhere,
and honestly a pretty cool party trick! Even if you don't like puzzles, I recommend you to try cubing.
It's fun, it's easy\*, and anyone of any age can enjoy it!

If you want to learn cubing, I recommend [this video by J-perm.](https://www.youtube.com/watch?v=7Ron6MN45LY) I learned to solve the 3x3 and the 5x5 cube
(the 5x5 is just a 3x3 with extra steps) by watching his videos.

### I know you have a cube at home

No you can't hide it. I know. Most of you guys have a cube at your home because of a reason or another.
I know I did, and I know many people do. Trust me, it's easy, it's fun. Pick that cube up, and follow this video.
Solve that cube. Trust me, you'll thank me later.

---
\* it's very easy to learn, you only need to remember some basic cases. But, the techniques the hardcore speedcubers use *are* somewhat hard to learn.
But you can have a lot of fun without learning those hard techniques!
