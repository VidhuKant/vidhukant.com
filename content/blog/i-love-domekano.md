+++
title = "I love Domestic Na Kanojo"
description = "Domestic Girlfriend is an unholy combination of incest and incest and incest yet it's the manga with the best character development."
date = 2023-01-05

[taxonomies]
tags = ["anime", "manga", "review"]
+++

I just realised the year has changed, and apparently it happened almost a week ago, so I thought I'd talk about the best manga I read in 2022.
And that manga is... Domekano. I know Domekano has a reputation, I mean the whole manga is focused around incest and some other forms of perversion. 
But there is something about the writing that makes it really satisfying to read.

## A little bit about Domestic Girlfriend

For those who are not familiar with the plot of Domestic Girlfriend, allow me to explain.
The main character, Natsuo has a crush on his teacher and doesn't look at other women. One day, his frends drag him to a mixer
and there he meets this girl named Rui, who takes him to her house and they end up having sex together just out of curiosity, and decide to forget about it.  
One day, Natsuo's dad says that he's going to have a second marriage and wants him to meet his potential new mother her daughters. Those future step sisters,
are in fact, Hina -Natsuo's teacher who he has a crush on-, and the other girl is Rui, the girl he lost his virginity to!
The three of these characters are forced to live in the same house, which is a very awkward setting and I won't spoil but there is a lot of incest involved.

## I love when you can feel the secondhand tension

The characters of Domekano are in a somewhat realistic situation. It's way too dramatic but it doesn't feel too much cliche. 
This manga covers both the hardships you might face after suddenly getting a step sibling (not that I can say much about that), 
and how drastically your living situation might change because of that. And also the feeling that the person you like is suddenly your family now.

### I love the characters
 
The characters are very well written, and are very realistic and very likeable. This goes for the supporting characters too.
The interaction between the characters of the Literature Club is a one of my favourite parts of the manga. Throughout the story you
can see a lot of character development. The characters fight a lot sometimes and that's what makes them really natural. 

I love how each character has their own issues. They worry about various stuff, about their future, about the person they like, about their
relationship with their friends, etc. I like how Natsuo works hard to become a writer, Momo tries to become a doctor and eventually is successful in that.
These small things make the story very interesting.

It's weird how the story covers Natsuo dating both of his step sisters and also covers how he gets inspiration for his stories from the people around him, 
and gives a good insight of a writer's life. I just love Natsuo's interactions with Shigemitsu-sensei and how he goes to different (sometimes shady) places with him,
to interview various people, and explores various aspects of people's lives as "research" for writing novels. 

### The character design is HOT

I mean, who wouldn't look at the characters and think that they aren't really attractive! Also Rui best girl. I so love the character design.


## The domestic relationship part makes it better somehow

From a slice of life point of view, Domestic Girlfriend is definitely in my top 10. It's kind of a mature slice of life story,
with not only happy but also sad moments, it's very inspiring at times, and also really heartwarming. But it also lots of incest. 
It's interesting how both serious moments and lots of perversion is packed into this one whole manga. From a romance point of view
and from a slice of life point of view, Domekano is really good. There is a lot of tension between the characters but they work it all out.

I do think that if this manga didn't have such an absurd setting it wouldn't really work, even though in many chapters this kind of setting
is totally irrelevant, it just wouldn't be as good. The absurt yet serious and realistic setting of Domekano makes it really good.
I'd love to buy the whole manga in paperback and read it again.
