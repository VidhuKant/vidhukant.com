+++
title = "ACTUALLY fix WiFi turning off on laptop lid close"
description = "If you have an HP laptop running linux you might have noticed that when you close the lid and open it again it turns off the WiFi."
date = 2023-09-12

[taxonomies]
tags = ["guide", "linux", "tech"]
+++

If you have an HP laptop running linux you might have noticed that when you close the lid and open it again it turns off the WiFi. 
Note that the weird part is that it turns off the WiFi after the lid *opens,* not when it's closed.

This is really weird and annoying because I constantly close the lid and open it again in class and stuff 
and sometimes I just don't wanna lose the network connection!

I searched on the internet and found a fix using setkeycodes. 

```
setkeycodes e057 240 e058 240
```

Everywhere I searched people were using the same custom systemd service
that apparently worked for them, but not for me. The fix worked, but not the systemd service.
After checking some logs, I found this:

```
hp-keycodes.service: Failed to open /etc/systemd/system/hp-keycodes.service: No such file or directory
```

Hmm so if I did a `systemctl start hp-keycodes` it worked but not with `systemctl enable`. Weird. 
It somehow couldn't find the .service file while booting up.

## The Fix

The is the correct systemd service that you need to use:

Store it as `/etc/systemd/system/hp-keycodes.service`.

```
[Unit]
Description=HP setkeycodes fix

[Service]
Type=oneshot
Restart=no
RemainAfterExit=no
ExecStart=/usr/bin/setkeycodes e057 240 e058 240

[Install]
WantedBy=multi-user.target
```

and just run `systemctl enable --now hp-keycodes.service`.

Now, when you close and re-open your laptop's lid, it won't toggle the WiFi.

*If this fix doesn't work for you, it probably means it's a different thing that's causing problems and you should research more instead of following this guide.*

## The problem with other fixes

On almost every website I saw the same .service file. What's different in mine is that it uses `WantedBy=multi-user.target` while the others had this:

```
[Install]
WantedBy=rescue.target
WantedBy=multi-user.target
WantedBy=graphical.target
```

I just removed `rescue.target` and `graphical.target` and it worked!
