+++
title = "All these \"tech companies\" are taking away your freedom."
description = "Why we should steer clear of large corporations and their data mining practises"
date = 2022-09-16

[taxonomies]
tags = ["rant", "tech"]
+++

Most people (speaking from my experience) don't even understand what freedom means. From the start they are taught to go with the flow. There are things that they must do, there are things they mustn't. And frankly they don't even care about it if they're getting what they want.
And the people who know better than that, are taking advantage of this. They want you to believe that this is the right thing. They're real manipulative people.

All of this applies to the tech sphere. I'm relating this to tech for two reasons: First being that this is my field of specialization, and second, because it's really ingrained in our lives now. It's at the point where it is very hard to back out. Which is concerning because seems like soon it'll become impossible to back out! That's **not** a good thing!

## You are just a pawn for all of these "tech companies"

Would you like to live in a world where people's opinion is greatly affected by just one individual?  
Or a world where nothing in your life can be kept private?  
A world where all of your actions are being dictated by others?  
Well you don't have to imagine it! You already are! how cool. It's only going to get worse.

You are living in a world of constant surveilance. Your phone number, more often than not, is tied to your legal ID.
When you sign up for... almost anything at this point, you are giving away your publically identifiable information.
Wherever you go, your mobile phone is scanning where you are, and sending this data to your phone company. 
Even dumb phones are subject to surveilance. The text messages you send to people, they're not private.

Many people argue that so what if, say, Discord knows what they're talking about with their friends? Or that they have "nothing to hide".
Which is just, irresponsible. You're literally throwing away some of your rights by saying "I have nothing to hide".

*"Privacy is not the same as secrecy. Everyone knows what happens in the toilet, still everyone locks the door."* - Unknown (I forgot)

### The data that you share will be used to manipulate you

The point is, that all these "tech" companies are data mining you. They know your daily activities, and the system has made you to believe that it's not a big deal.
They know your vulnerabilities, they know your likes and dislikes. They are the connoisseurs of social engineering.
When you are giving out all this data to others, you have to understand the liabilities of it.
This data can very well be used against you. 

#### Targeted advertisements

Targeted marketing is a very serious thing. These businesses feed on your insecurities, (which you cannot hide! The Gods of AI see everything)
they are constantly searching for ways to make you buy their product. They are very good at making you believe that you absolutely need this thing.

#### Social platforms, etc use this data to get you addicted to their service

This is one of the most concerning things I see with people around me. Take YouTube for example, when you watch a video, it would recommend you other videos related to it. These recommendations aren't random, they are mostly based on what you have seen in the past. Their ML algorithm is very good at analysing what your likes are depending on what you've watched before.
This, on the outside, looks like very convenient technology. I mean shit, I love when they recommend me music I like, even I'm not immune to this. But people just overlook the dark side of it. I've said this before but this is a really good technique to get you addicted to their service.
They're want you to get the dopamine rush you need. You watch one video, they give you another until you get immune to the dopamine, and consume more and more of their content, until you get addicted.

#### You can be held liable for everything you've said

Your phones are constantly listening. They record everything that you talk about, and they send it to your phone company.
What's worse is that now we have all these voice activated "smart" devices, which ~~you can use to turn on the lights with your voice~~ can use *you* for their profits.
On the surface this doesn't seem too bad, but you have to realise that the things you say now, the places you go to now, they are recorded and in the future you can totally be held liable for that.

There was this incident about a student who said something like he'd commit arson if he loses this game or something like that. I'm not exactly sure what it was, but he was just chatting with his friends while playing some online game. He was most likely just joking but 3 years later the recording somehow got out, and he was taken to court. He didn't even do anything, but he was taken to court and I'm not sure if he's still in there but he had to spend time in jail.  
And the judge literally said that he doesn't know what it means to "download" something (he probably said something like that in the recording), yet he was jailed.

This could happen to YOU and imagine how much worse it could become in the next few years. Some people are installing smart devices in like, every room of their houses.
That was just a voice chat, a smart device listening to you is bound to be so much worse.

For those people who believe that they don't keep their recordings "because they said so", you literally have no proof. You can NEVER trust software unless it is Free. Nonfree software can never, in any way, can be trusted. Unless your IoT device is librebooted, be ready to get in deep trouble for a dumb reason like 5 years later.

### You are not in control of the things that you own

#### Companies selling features as services are bullshitting you

We have reached a ridiculous point where you (obviously) have to pay for something you buy, and **you need to pay extra to use it.**

I recently read about a new BMW car (I forgot the model but you can look it up pretty easily on the internet), which comes with heated seats, but you have to pay extra to use the heated seats. Now you might be thinking, they charge extra for the heated seats because probably most people don't need it and it's expensive so they did this to reduce the car's cost.
Boy, are you wrong. This fucking car, comes with heated seats. When you pay for the car, you pay for the heated seats. You own the seats. But you need to pay them extra *on a monthly basis* to use those seats. What kind of idiot would be okay with this? This is not ethical. They've realised that people will pay for literally anything if they sugarcoat it. This is what they think about their customers. That they're dumb enough to buy everything they sell. 

Tesla also did something like this a few years ago! (not sure if they still are doing this kind of thing)
Their lower end cars would come with all those cameras and stuff but it won't come with autopilot mode enabled. But you need to pay $5000 to get the feature enabled.
You. paid. for. the. equipment. Now you need to pay $5000 to use it. Imagine buying a house, but you need to pay the property dealer extra so they can unlock the kitchen.

The main point is, you're not paying to get the kitchen built, you're paying to use the pre-built kitchen you already paid for.

## Your opinions are not yours anymore

The internet influences a lot of us. Sometimes in good ways I must say, but the downsides can't help but take over the good sides.
The media really wants to force their opinions down your throat. They want you to believe in things that would benifit them. It's worse than ever because
media is now greatly controlled by people who *make* these devices you use to access the internet. They have the authority to control what is being shown to you, and what isn't. You're totally not free. You are in virtual jails created by huge corporations, filled with targeted advertising, and social engineering.
