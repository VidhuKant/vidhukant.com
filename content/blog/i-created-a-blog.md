+++
title = "I Created a Blog!"
description = "My first blog post on this website."
date = 2022-07-12

[taxonomies]
tags = ["updates"]
+++

## Yos I have finally done it

This is sort of like a test for my blog, generated with hugo, pog stuff.
This has been a journey. This is like the third iteration of my blog. First one was blog.vidhukant.xyz which no longer exists.
It was not a static site, it was a server side rendered web app written in go. It looked awesome but I got bored pretty quickly.
(also writing markdown is better)

Second time I decided to just write my blog in plain html which was a horrible, horrible idea and now on third iteration now we have this! A static site built with hugo.

## The "philosophy" behind this blog

Seems cool doesn't it? 

Yea that's it. I like talking to likeminded people online, but social media is tiring ~~(and evil!)~~ so I just created my own blog! 
Hopefully I write at least a few useful posts. I totally see this going into either a doujin review site or me just shilling free software.

## What are these GIFs on the bottom?

This seems to be a trend from the 90s, you'd add different GIFs linking to other websites that you like.
I wasn't even born at that time so I obviously haven't seen this trend in all of its glory, but hey these things are fun so I'll add it.
If you also have a website (preferably a "minimal" one) (wait does this one count as minimal?) do add my "web button" to your site!

## Thanks to landchad.net

I learned many things from landchad.net, like setting up a nginx frontend server, firewalls, rsync and stuff. Really neat site, I recommend checking out.
Also lol check out my [youtube channel](https://youtube.com/@_VidhuKant) because why not. At this moment there aren't any videos because
I deleted my old channel and this is a brand new one. But stay tuned for some quality content!

## Check out the latest projects I've been working on

[MAL2Go](/docs/mal2go/v4) is a very neat API Wrapper that lets you use the MyAnimeList API using Go. 
[macli](/docs/macli) is an awesome CLI-Based BASED MyAnimeList unofficial client so you don't have to look at a web page (eww)

And I'm already shilling software. 
