+++
title = "Customizing a Doom Emacs Theme"
description = "How to create your own Doom Emacs theme"
date = 2022-12-02
highlight = true 

[taxonomies]
tags = ["emacs", "guide", "tech"]
+++

Creating your theme/modifying an existing one, or overriding some faces (globally) in Emacs, especially Doom Emacs is
really easy once you understand how to do it... but it wasn't very easy to *understand* how to do it. Most likely
I was doing something wrong, or maybe it's just because I don't fully know how lisp or emacs works that's why it took me long
but I spent a good part of my evening trying to make even small changes to work.

So, I have created this short tutorial to leave me (and others having problems) some notes on how to modify a Doom Emacs theme.
I'm using Doom Emacs which comes with the doom-themes packages doing some basic setup so we only need to define some variables and it 
automatically applies other faces and stuff, and I'm pretty sure doom-themes can be installed on regular Emacs.

## Overriding faces

Each element in an emacs buffer has a "face" which defines its foreground/background color, font styling, etc.
You can do `M-x RET` `describe-char` or `describe-face` to get the face of the area under the cursor, or to get a
list of all the available faces (which is very long)

The `custom-set-faces!` macro (or `custom-set-faces` for Emacs users) can be used to customize any face:

``` lisp
(custom-set-faces!
  '(default :background "#100b13")
  '(cursor :background "#0ec685" :foreground)
  '(line-number :slant normal :background "#100b13")
  '(line-number-current-line :slant normal :background "#21242b"))
```

You can add something like this to your `~/.doom.d/config.el`

## Using a doom theme as a template

Another way to modify your Doom theme is to use an existing theme as a template (or, starting from bottom up!)
and modifying it.

### How to modify an existing theme:

1. Go to [this page](https://github.com/doomemacs/themes) and choose any theme you like, and download the raw file into
`~/.doom.d/themes/<theme-name>-theme.el`. The theme name can be anything, but make sure it ends with "-theme.el" 
or Doom won't recognise it as a theme.

2. Open the theme in your favourite text editor (I wonder which one it is) and edit the line that says `(def-doom-theme <theme-name>`
and replace `<theme-name>` with any name you like, make sure not to use the original name (or the name of any other theme that already exists on your system) or it would create a clash. 
Now, edit the theme to your liking and you're good to go!

3. Open a new Doom Emacs frame and enter `SPC h t t` and select your new theme!
