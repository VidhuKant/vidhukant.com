+++
title = "Website Domain Change (and letting go of weeb-developerz.xyz)"
description = "domain change for this website (and letting a domain expire)"
date = 2023-05-10

[taxonomies]
tags = ["updates"]
+++

If you've been a reader of this website for more than a few months ago, you might know that I have 3 domains:

- vidhukant.xyz
- mikunonaka.net
- weeb-developerz.xyz

Being the domain-hoarder I am, I got another domain!!! It's vidhukant.com because of course everyone wants a .com domain nowadays.

I am changing the website's main domain to vidhukant.com because of course I am. 
This won't really change anything since I don't plan to ever cancel vidhukant.xyz, and it'll always be redirecting to vidhukant.com.
For now mikunonaka.net still redirects to this website but I plan to change that one day (yes I plan to maintain 2 websites im weird)

Also, I no longer have weeb-developerz.xyz because my allowance only supports 3 domains. If there is any activity on weeb-developerz.xyz, please note that
it's not me. I know just letting go of a domain like that is not that good of an idea, but fuck it.
For my email, if anyone is wondering, it doesn't matter if you write to me on vidhukant.xyz or vidhukant.com, They're all reaching the same mailbox anyways.
