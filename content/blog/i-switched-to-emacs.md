+++
title = "I Switched to (Doom) Emacs"
description = "After much thought I have switched my default text editor/IDE from NeoVim to Doom Emacs."
date = 2022-11-26

[taxonomies]
tags = ["emacs", "programming", "tech"]
+++

After [failing to set up gentoo](/blog/2022/my-failed-attempt-at-installing-gentoo/) I've decided
to stop using linux. After thinking about it for some time I finally installed emacs. Now instead of
Arch Linux, I have an entry for emacs in my GRUB config.

Jokes aside, emacs is actually an awesome text editor! Though I think `C-x C-f` is bullshit. Evil mode
is the greatest emacs extention! I've been using [Doom Emacs](https://github.com/doomemacs/doomemacs) which is an emacs "configuration framework" which comes
with Evil mode turned on by default. Idk if it counts as cheating but I don't really care.

Honestly, getting used to emacs was not hard but not easy either. It was somewhere in the middle.
It wasn't too hard probably because I had experience with vim. But apart from having similar keybindings with Evil mode,
and having kind of the same philosophy of never leaving your keyboard, emacs is much different. 

## It's one of the most polished IDE I've used

Okay, the only other IDE I've used is IntelliJ Idea, so I don't think I have too much experience, but Doom Emacs is really the most fun
text editor/IDE to code in. It has support for multiple programming languages, which is the most important for me, but it also has
a great way of navigating around the projects and also has git integration that I really haven't used much.

Unlike vim, it *does* have long startup times but apart from that, it's pretty fast and has LSP support 
(especially Doom Emacs which has some bundled packages for lots of languages), and it just feels much more polished
as compared to something like CoC.

## Emacs can literally do anything

The only thing keeping me from directly booting into emacs is my XMonad config which is I'm way too comfortable with.
I mean, Emacs can act as a text editor/IDE, an image viewer, PDF viewer, web browser, music player, email client, RSS reader, and A WINDOW MANAGER

That's pretty cool! I might try exwm (I think that's what it's called) when I get more comfortable with emacs..

## Try out Emacs NOW!

Emacs is really innovative, it feels like home because of the vim emulation but with a plethora of extra features.
Since emacs manages its own internal buffers, as a tiling window manager user I found it kinda hard to get used to,
but in the end I feel much more productive. I haven't even tried out most of the features I know about, like I have changed like 5 lines
in the config file so there is still a lot for me to learn, but it's very fun!

Even if you don't have much experience with vim, I recommend trying out emacs. I think apart from the weird buffer/window thing and 
the keyboard-driven interface, it still feels much closer to things like VSCode, not vim.
