+++
title = "Stream system audio to your friends over discord! (and other applications)"
description = "This script automatically sets up a virtual sink with loopback from your speakers and mic that can be used in online conferencing software."
date = 2023-10-15
highlight = true

[taxonomies]
tags = ["guide", "linux", "tech"]
+++

On linux there isn't any way to screen share with audio on popular video chat applications. Well who cares if the application doesn't support it!
With this script you have a finer control over what others hear. You can choose between your sound, system sound, both and 
can even chose which system sounds others can hear and what others can't!

This is supposed to work on both pulseaudio and pipewire, I haven't tried with pulse so just in case if you have any issues, [please let me know.](/contact)

``` sh
#!/bin/sh

default_source="$(pactl get-default-source)"
default_sink="$(pactl get-default-sink)"

# create virtual sinks
pactl load-module module-null-sink sink_name=VirtualSpeaker
pactl load-module module-null-sink media.class=Audio/Duplex sink_name=VirtualMicrophone

# create loopbacks
pactl load-module module-loopback source="VirtualSpeaker.monitor" sink="$default_sink"
pactl load-module module-loopback source="$default_source" sink="VirtualMicrophone"
pactl load-module module-loopback source="VirtualSpeaker.monitor" sink="VirtualMicrophone"

# set default mic
pactl set-default-source "VirtualMicrophone"
```

After running this script you'll be able to choose VirtualMicrophone as the mic in any chat app; 
the last line of this script also ensures that this new mic is the default mic, you can remove this if you want.

## How does this work?

This script basically creates a virtual speaker and a virtual microphone. You can listen to the virtual speaker on your physical speakers, while the virtual mic is always listening to both your virtual speaker and your virtual mic. So basically your voice and the playback is merged into this new mic.

Using pavucontrol you can change the playback device for each stream differently from the playback tab.

## Further configurations

- Choosing what others hear:

    If you have youtube (or any other app) playing in the background, you can use pavucontrol to select its output device from the playback tab.
    
    - When the playback is set to your headphones/speakers, only you can hear it:
    ![Pavucontrol playback to headphones](/pics/vmic_playback-headphones.webp)

    - When the playback is set to VirtualSpeaker, both you and others can hear it through your microphone.
    ![Pavucontrol playback to VirtualSpeaker](/pics/vmic_playback-virtualspeaker.webp)

    - When the playback is set to VirtualMicrophone, only others can hear it.
    ![Pavucontrol playback to VirtualMicrophone](/pics/vmic_playback-virtualmic.webp)
    
    *you can have multiple audio streams playing being routed to different devices through pavucontrol*
    
- Setting volume:
    - Setting the volume of your speakers only changes the volume for you
    - Setting the volume of your virtual speaker changes the volume for everyone. You can do so from the output devices tab.
    - To only change the volume for *others*, go to the recording tab, and set the show option at the bottom to either "All" or "Virtual Streams"
    ![Pavucontrol recording devices](/pics/vmic_recording.webp)
        Usually the first loopback device routes VirtualSpeaker to VirtualMicrophone. Changing its volume level will only affect others.

## Common Issues

Please [report](/contact) any other issues to me! I'll update this page for fixes if I think the issue worthy enough.

- Not hearing any sound:

    Sometimes any of the new virtual devices created by this script might be muted by default. 
    Go to the recording tab and set the show option to virtual streams, and make sure all 3 of the loopbacks are unmuted.
    Do the same for VirtualMicrophone and VirtualSpeaker from the "Output Devices" tab.
