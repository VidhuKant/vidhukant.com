+++
title = "This site's getting rusty"
description = "I recently re-designed my website and this time I used a different static site generator called Zola. Before, I was using Hugo."
date =  2024-05-03T10:30:00+05:30

[taxonomies]
tags = ["tech", "updates"]
+++

Up until now, my site was *going* well but now it's gotten pretty *rusty*. 
*You see what I did there?*

Yes, dear reader, we have switched from [Hugo](https://gohugo.io), the static
site generator I've been using for almost two years. I still love Hugo, it's very
fast and it's written in my favourite programming language; and I'm familiar enough
with go's template engine, so Hugo was a breeze.

BUT it's time for Go to go, because rust is hip nowadays. So, my friends, I re-wrote
this site in.. [Zola](https://getzola.org)! Zola is a static site generator written in rust
and while Hugo claims to be "the fastest", Zola is a big contender. 
I obviously still have the files from the Hugo site, so I just did a quick test by
running `hugo` and `zola build`; it might be inaccurate but it took them 45ms and 48ms respectively
to build my website! Note that my Hugo theme is slightly larger!

I wanted to redesign my website anyways (it's MUCH nicer now) so I just went and did this in
Zola as I thought I'd also learn a new tool in the process. I was going for Jekyll but then 
randomly came across Zola, and I'm very happy I did. Zola is so much simpler than Hugo
and I got the site converted from Hugo to Zola very quickly. 

I'd definitely consider Zola for later projects; but my existing hugo sites (except this one)
will probably continue to be in hugo, because it'd be a waste of time without any real 
advantage converting them.
