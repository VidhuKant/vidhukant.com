+++
title = "You can't have a good story without great characters"
description = "Let me explain, a good story is one that keeps the reader engaged. You need to seek a reaction out of them. To do that, your characters need to be plausible..."
date = 2022-09-29

[taxonomies]
tags = ["opinion"]
+++

I am no writer myself, but I love stories, and I think I can *somewhat* critique them.
And one of my main complaints with most stories (whether it be a book, or an anime, etc) is the story being
too *steriotypical*. Nowadays, you read one book, pick up another from the same genre and there's a very good
chance it's just a clone of the previous one you read. To some extent it's acceptable because you can't write a
story based on nothing, can you?

I believe you cannot completely fabricate a story. Take the fantasy/time travel genre for an example,
you totally can predict what's going to happen most of the time. It's honestly really overdone. I think not being predictable is also 
a very important part of writing, but I won't be covering that. What really matters is how the characters
react to something, and how the reader/viewer reacts to *that*.

Let me explain, a good story is one that keeps the reader engaged. You need to seek a reaction out of them.
To do that, your characters need to be *very* plausible. If your characters aren't well written, it really doesn't
matter how good your plot is. Every reader knows what's going to happen in a romance novel, but we still read them.
Why is that? Because the selling point of a story is not what's gonna happen, but how the characters are going to 
react to it. If you have a half assed, not well written character, no one's going to like the story.
But, if your character is very well written, and very detailed and plausible, that's going to make the reader
sad when the character is sad, or happy when the character is happy. Such a character 
makes it easier for the reader to imagine the character as a real person.

And I think that's the most important part of a story. If the reader reads the story and feels nothing,
it's absoluely useless. Imagine if in a story a character dies but it doesn't matter because the reader
just didn't bond with the character. That'd be a truly boring story.

It's just something I've been thinking about, so I thought I'd share this,
but if you're into writing, make sure that your characters are extremely well written.
Emotional attachment with the reader is extremely important (probably applies to all kinds of 
creative work!), which seems to be overlooked nowadays.
