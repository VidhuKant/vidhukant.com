+++
title = "The Guitar Is Pretty Neat"
description = "The guitar has always been my favourite musical instrument, and it's been almost 3 years since I planned to buy an acoustic guitar."
date = 2023-03-10

[taxonomies]
tags = ["music", "other"]
+++

The guitar has always been my favourite musical instrument, and it's been almost 3 years since I planned to buy an acoustic guitar.
Due to circumstances and stuff, I sadly never got one.  
That was until I watched Bocchi The Rock! There is some power in cute girls doing cute things that can drive a man to do anything.
So I finally got a guitar! And because bocchi has one, obviously I got an electric guitar (though it's not the same as bocchi, it's a strat because I don't like les paul)

I have no idea how to play a guitar, and while I didn't expect it to be easy, I thought I could probably get by with just YouTube tutorials.
Well, that's not the case! So I guess I need guitar lessons. I don't live in a very big town so we'll see how that goes...

I don't think I'd have much issues keeping myself motivated to learn the guitar, because more than anything, I love vibing to my favourite anime openings.
It's gonna be so dope when I get good enough to play all the songs from Bocchi The Rock... and even Hikaru Nara! Maybe I'll keep the blog updated with my guitar progress
