+++
title = "Don't Cover Liabilities With Liabilities"
description = "You'd find many products, etc that promise to fix an issue, sometimes they actually do. But many times they're just trying to make you believe that you're...."
date = 2022-10-02

[taxonomies]
tags = ["opinion", "other"]
+++

Robert Kiyosaki, in his famous book, "Rich Dad, Poor Dad", said 
"You must know the difference between an asset and a liability, and buy assets."
I like that statement. I don't think it only applies to making money.

Not only people don't realise that they're constantly piling liabilities,
they use liabilities to... get rid of liabilities?

## Liabilities that take liabilities away

You'd find many products, etc that promise to fix an issue, sometimes they actually do.
But many times they're just trying to make you believe that you're getting rid of all these problems,
while in fact, these things that promise to take away your problems *are* a problem themselves.

I stumbled across one of these recently. It was a certain app that scans all of your emails and gives you a
report on what websites and services you've registered with and which sites are keeping your personal data.
Apparently it also lets you easily request to delete that personal data. How convenient!

The problem is all these companies keeping a hold of your personal data. 
The goal is to keep track and/or ask to delete that data.
Now you're giving **another** company all of your data, which analyses your data and tells 
you about who is using your data. People falling for this have completely missed the point.
You *think* you are solving a problem, but you're making the problem bigger!

You initially wanted to lower your digital footprint or whatever, but instead
you signed up for yet another service, which **increases** your digital footprint.
Beware of such tactics. ~~For legal reasons~~ I can't say that the app is doing something bad,
but you need to realise that what you think will take liabilities away might even be a liability itself.
